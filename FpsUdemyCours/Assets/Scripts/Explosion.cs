﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public int damage = 50;

    public bool damageEnemy, damagePlayer;

    public ParticleSystem explosionPart;
    
   

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Enemy"&& damageEnemy)
        {
            other.GetComponent<EnemyHealthController>().DamageEnemy(damage);
          
        }

        if(other.tag=="Player" && damagePlayer)
        {
            other.GetComponent<PlayerHealthController>().DamagerPlayer(50);
            
            
        }

      //  Destroy(gameObject, 10f);
    }
}
