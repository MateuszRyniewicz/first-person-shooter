﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VictoryScreen : MonoBehaviour
{

    public string mainMenu;

    public float timeBeetwenShowing = 1f;

    public GameObject textBox, ManuButton;

    public Image blackScreen;
    public float blackScreenFade = 2f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ShowObjectCo());

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, Mathf.MoveTowards(blackScreen.color.a, 0f, blackScreenFade*Time.deltaTime));    
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }
    
    public IEnumerator ShowObjectCo()
    {

        yield return new WaitForSeconds(timeBeetwenShowing);

        textBox.gameObject.SetActive(true);

        yield return new WaitForSeconds(timeBeetwenShowing * 2);

        ManuButton.gameObject.SetActive(true);
    }

}
