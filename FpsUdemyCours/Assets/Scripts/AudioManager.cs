﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;

    private void Awake()
    {
        instance = this;
    }

    public AudioSource Bdm, victory;

    public AudioSource[] soundsEffects;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BDMstop()
    {
        Bdm.Stop();
    }
    public void PlayLevelVictory()
    {
        BDMstop();
        victory.Play();
    }

    public void PlaySFX(int SFXnumber)
    {
        soundsEffects[SFXnumber].Stop();

        soundsEffects[SFXnumber].Play();
    }

    public void StopSFX(int SFXnumber)
    {
        soundsEffects[SFXnumber].Stop();
    }
    

    
}
