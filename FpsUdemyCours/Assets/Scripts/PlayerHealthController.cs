﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{

    public static PlayerHealthController instance;

    public float invincibleLenght = 1f;
    private float invincibleCounter;

    private void Awake()
    {
        instance = this;
    }


    public int maxHealth, currentHealth;



    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;

        UiController.instance.healthSlider.maxValue = maxHealth;
        UiController.instance.healthSlider.value = currentHealth;
        UiController.instance.healthText.text = "HEALTH: " + currentHealth + "/" + maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (invincibleCounter > 0)
        {
            invincibleCounter -= Time.deltaTime;
        }   
    }

    public void DamagerPlayer( int damageAmount)
    {

        if (invincibleCounter <= 0 && !GameManager.instance.levelEnding)
        {
            AudioManager.instance.PlaySFX(7);

            currentHealth -= damageAmount;

            UiController.instance.ShowDamage();



            if (currentHealth <= 0)
            {
                gameObject.SetActive(false);

                currentHealth = 0;

                GameManager.instance.PlayerDied();

                AudioManager.instance.BDMstop();

                AudioManager.instance.PlaySFX(6);

                AudioManager.instance.StopSFX(7);
            }

            invincibleCounter = invincibleLenght;

            UiController.instance.healthSlider.value = currentHealth;
            UiController.instance.healthText.text = "HEALTH: " + currentHealth + "/" + maxHealth;
        }
    }

    public void HealPlayer(int healAmount)
    {
        currentHealth += healAmount;

        if (currentHealth > maxHealth)
        {
            maxHealth = currentHealth;
        }

        UiController.instance.healthSlider.value = currentHealth;
        UiController.instance.healthText.text = "HEALTH: " + currentHealth + "/" + maxHealth;
    }
}
