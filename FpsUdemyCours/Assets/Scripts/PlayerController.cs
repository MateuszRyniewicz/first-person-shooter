﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;



    private void Awake()
    {
        instance = this;
    }





    public float moveSpeed, gravityModifier, jumpPower, runSpeed = 12f;
    CharacterController characterController;


    Vector3 moveInput;

    public Transform camTrans;

    public float mouseSensitivity;
    public bool invertX;
    public bool inverY;

    private bool canJump, canDubleJump;
    public Transform groundCheckPoint;
    public LayerMask whatIsGrounded;

    private Animator anim;


   // public GameObject bullet;
    public Transform firePoint;


    public Gun activeGun;
    public List<Gun> allGuns = new List<Gun>();
    public List<Gun> unlockableGuns = new List<Gun>();
    public int currentGun;

    public Transform adsPoint, gunHolder;
    private Vector3 gunStartPos;
    public float AdsSpeed = 2f;

    public GameObject muzzleFlash;

    public AudioSource footsStepSlow, footsStepFast;

    private float bounceAmount;
    private bool isBounce;

    public float maxViewAngle = 60f;
   

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();

        currentGun--;

        SwitchGun();

        gunStartPos = gunHolder.localPosition;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!UiController.instance.pauseSceen.activeInHierarchy && !GameManager.instance.levelEnding)
        {


            // moveInput.x = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
            // moveInput.z = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;


            // store y velocity
            float yStore = moveInput.y;

            Vector3 vertMove = transform.forward * Input.GetAxis("Vertical");
            Vector3 horiMove = transform.right * Input.GetAxis("Horizontal");

            moveInput = vertMove + horiMove;
            moveInput.Normalize();

            if (Input.GetKey(KeyCode.LeftShift))
            {
                moveInput = moveInput * runSpeed;
            }
            else
            {
                moveInput = moveInput * moveSpeed;
            }


            moveInput.y = yStore;

            moveInput.y += Physics.gravity.y * gravityModifier * Time.deltaTime;

            if (characterController.isGrounded)
            {
                moveInput.y = Physics.gravity.y * gravityModifier * Time.deltaTime;
            }


            canJump = Physics.OverlapSphere(groundCheckPoint.position, 0.3f, whatIsGrounded).Length > 0;

            if (canJump)
            {
                canDubleJump = false;
            }


            // Handle Jumping

            if (Input.GetKeyDown(KeyCode.Space) && canJump)
            {
                moveInput.y = jumpPower;

                canDubleJump = true;

                AudioManager.instance.PlaySFX(8);

            }
            else if (Input.GetKeyDown(KeyCode.Space) && canDubleJump)
            {
                moveInput.y = jumpPower;

                canDubleJump = false;

                AudioManager.instance.PlaySFX(8);
            }

            if (isBounce)
            {
                isBounce = false;
                moveInput.y = bounceAmount;

                canDubleJump = true;

            }


            characterController.Move(moveInput * Time.deltaTime);


            // mouse Move;

            Vector2 mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")) * mouseSensitivity;

            if (invertX)
            {
                mouseInput.x = -mouseInput.x;
            }

            if (inverY)
            {
                mouseInput.y = -mouseInput.y;
            }


            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + mouseInput.x, transform.rotation.eulerAngles.z);

            camTrans.rotation = Quaternion.Euler(camTrans.rotation.eulerAngles + new Vector3(-mouseInput.y, 0f, 0f));

            if (camTrans.rotation.eulerAngles.x > maxViewAngle && camTrans.rotation.eulerAngles.x < 180f)
            {
                camTrans.rotation = Quaternion.Euler(maxViewAngle,camTrans.rotation.eulerAngles.y, camTrans.rotation.eulerAngles.z);
            }
            else if(camTrans.rotation.eulerAngles.x > 180f && camTrans.rotation.eulerAngles.x<360f - maxViewAngle)
            {
                camTrans.rotation = Quaternion.Euler(-maxViewAngle, camTrans.rotation.eulerAngles.y, camTrans.rotation.eulerAngles.z);
            }

            muzzleFlash.gameObject.SetActive(false);
            // Handle Shooting
            // single shoots
            if (Input.GetMouseButtonDown(0) && activeGun.fireCounter <= 0)
            {
                RaycastHit hit;
                if (Physics.Raycast(camTrans.position, camTrans.forward, out hit, 50f))
                {
                    if (Vector3.Distance(camTrans.position, hit.point) > 2f)
                    {


                        firePoint.LookAt(hit.point);
                    }
                }
                else
                {
                    firePoint.LookAt(camTrans.position + (camTrans.forward * 30f));
                }


                // Instantiate(bullet, firePoint.position, firePoint.rotation);
                FireShot();
            }


            //repeating shot
            if (Input.GetMouseButton(0) && activeGun.canAutoFire)
            {
                if (activeGun.fireCounter <= 0)
                {
                    FireShot();
                }
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                SwitchGun();
            }

            if (Input.GetMouseButtonDown(1))
            {
                CameraController.instance.ZoomIn(activeGun.zoomAmount);
            }

            if (Input.GetMouseButton(1))
            {
                gunHolder.position = Vector3.MoveTowards(gunHolder.position, adsPoint.position, AdsSpeed * Time.deltaTime);
            }
            else
            {
                gunHolder.localPosition = Vector3.MoveTowards(gunHolder.localPosition, gunStartPos, AdsSpeed * Time.deltaTime);
            }



            if (Input.GetMouseButtonUp(1))
            {
                CameraController.instance.ZoomOut();
            }


            anim.SetFloat("moveSpeed", moveInput.magnitude);
            anim.SetBool("onGrunded", canJump);

        }
    }

    public void FireShot()
    {
        if (activeGun.currentAmmo > 0)
        {


            activeGun.currentAmmo--;
            Instantiate(activeGun.bullet, firePoint.position, firePoint.rotation);

            activeGun.fireCounter = activeGun.fireRate;


            UiController.instance.ammoText.text = "AMMO: " + activeGun.currentAmmo;
            muzzleFlash.gameObject.SetActive(true);

        }
    }

    public void SwitchGun()
    {

        activeGun.gameObject.SetActive(false);

        currentGun++;

        if(currentGun >= allGuns.Count)
        {
            currentGun = 0;
        }


        activeGun = allGuns[currentGun];
        activeGun.gameObject.SetActive(true);

        UiController.instance.ammoText.text = "AMMO: " + activeGun.currentAmmo;

        firePoint.position = activeGun.firepoint.position;
    }


    public void addGun(string gunToAdd)
    {
        bool gunUnloced = false;

        if (unlockableGuns.Count > 0)
        {
            for( int i = 0; i < unlockableGuns.Count; i++)
            {
                if(unlockableGuns[i].gunName == gunToAdd)
                {
                    gunUnloced = true;

                    allGuns.Add(unlockableGuns[i]);

                    unlockableGuns.RemoveAt(i);

                    i = unlockableGuns.Count;
                }
            }
        }

        if (gunUnloced)
        {
            currentGun = allGuns.Count - 2;
            SwitchGun();
        }

    }

    public void Bounce(float bounceForce)
    {
        bounceAmount = bounceForce;
        isBounce = true;
    }

}
