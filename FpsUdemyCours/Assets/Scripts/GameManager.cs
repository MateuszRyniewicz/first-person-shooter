﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    private void Awake()
    {
        instance = this;

    }

    public float waitAfterDying = 2f;

    [HideInInspector]
    public bool levelEnding;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
           

            PauseUnpause();
        }
    }

    public void PlayerDied()
    {
        StartCoroutine(PlayerDiedCo());
        
    }

    public IEnumerator PlayerDiedCo()
    {
        yield return new WaitForSeconds(waitAfterDying);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    } 


    public void PauseUnpause()
    {
        if (UiController.instance.pauseSceen.activeInHierarchy)
        {
            UiController.instance.pauseSceen.SetActive(false);

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            Time.timeScale = 1;

            PlayerController.instance.footsStepFast.Play();
            PlayerController.instance.footsStepSlow.Play();
        }
        else
        {
            UiController.instance.pauseSceen.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            Time.timeScale = 0f;

            PlayerController.instance.footsStepFast.Stop();
            PlayerController.instance.footsStepSlow.Stop();
        }
    }
}
