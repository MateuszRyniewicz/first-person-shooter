﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float moveSpeed, lifeTime;

    private Rigidbody rb;

    public GameObject impactEffect;

    public int damage = 1;

    public bool damageEnemy, damagePlayer;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = transform.forward * moveSpeed;

        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && damageEnemy)
        {

            other.gameObject.GetComponent<EnemyHealthController>().DamageEnemy(damage);

           // Destroy(other.gameObject);
        }

        if(other.tag == "Player" && damagePlayer)
        {
            PlayerHealthController.instance.DamagerPlayer(damage);
        }

        if(other.tag == "HeadShot" && damageEnemy)
        {
            other.transform.parent.gameObject.GetComponent<EnemyHealthController>().DamageEnemy(damage * 2);
            Debug.Log("HeadShot");
        }


        Destroy(gameObject);
        Instantiate(impactEffect, transform.position + transform.forward * (-moveSpeed * Time.deltaTime), transform.rotation);
    }
}
